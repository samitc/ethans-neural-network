//Ethan Alexander Shulman 2017

using EthansNeuralNetwork;
using System.Drawing;
using System.Collections.Generic;
using System.Drawing.Imaging;


namespace MessingAround
{
    class ImageGenerator
    {

        private const int COLOR_PRECISION = 3,
                          COLOR_AREA = COLOR_PRECISION*COLOR_PRECISION*COLOR_PRECISION;


        public /*static*/ void Main(string[] args)
        {
            //load example image
            Bitmap exampleImage = new Bitmap(args[0]);
        
            //create neural network
            NeuralNetwork neuralNet = new NeuralNetwork(new NeuralNetworkLayer(COLOR_AREA, false, null),
                                                        new NeuralNetworkLayer[] {
                                                            new NeuralNetworkLayer(COLOR_AREA*10, true, Utils.Rectifier_ActivationFunction)
                                                        },
                                                        new NeuralNetworkLayer(COLOR_AREA, false, Utils.Rectifier_ActivationFunction));
            neuralNet.RandomizeWeightsAndBiases(0.0f, 0.0f, 0.0f, 0.5f / (COLOR_AREA*10));
            
            //convert example image to training data
            float[][] inDat = new float[exampleImage.Height * exampleImage.Width - 1][],
                      targetDat = new float[inDat.Length][];
            BitmapData bdat = exampleImage.LockBits(new Rectangle(0,0,exampleImage.Width,exampleImage.Height), ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
            unsafe
            {
                byte* px = (byte*)bdat.Scan0.ToPointer();
                int stride = bdat.Stride;

                int ind = 0;
                for (int y = 0; y < exampleImage.Height; y++)
                {
                    for (int x = 0; x < exampleImage.Width; x++)
                    {
                        if (x == exampleImage.Width - 1 &&
                            y == exampleImage.Height - 1) continue;//skip last

                        int pxInd = x * 3 + y * stride;

                        float ir = px[pxInd] / 255.0f,
                            ig = px[pxInd+1] / 255.0f,
                            ib = px[pxInd+2] / 255.0f,
                            tr, tg, tb;
                        if (x < exampleImage.Width - 1)
                        {
                            pxInd += 3;
                        }
                        else
                        {
                            pxInd = (y + 1) * stride;
                        }
                        tr = px[pxInd] / 255.0f;
                        tg = px[pxInd + 1] / 255.0f;
                        tb = px[pxInd + 2] / 255.0f;

                        float[] ia = new float[COLOR_AREA];
                        Utils.Fill(ia, 0.0f);
                        ia[ColorToNeuronID(ir,ig,ib)] = 1.0f;

                        float[] ta = new float[COLOR_AREA];
                        Utils.Fill(ta, 0.0f);
                        ta[ColorToNeuronID(tr, tg, tb)] = 1.0f;

                        inDat[ind] = ia;
                        targetDat[ind] = ta;

                        ind++;
                    }
                }
            }
            exampleImage.UnlockBits(bdat);


            //get ready for training/generating
            NeuralNetworkProgram neuralProgram = new NeuralNetworkProgram(neuralNet);

            NeuralNetworkTrainer trainer = new NeuralNetworkTrainer(neuralNet, inDat, targetDat, 2, NeuralNetworkTrainer.LOSS_TYPE_AVERAGE);
            trainer.desiredLoss = 0.0f;
            trainer.lossSmoothing = 0.0f;
            trainer.learningRate = 0.04f;
            trainer.stochasticSkipping = false;


            //train/generate
            trainer.StartInit();

            long lastTime = 0,
                nextTime;
            while (true)
            {
                //train
                trainer.Learn();

                //generate every 10 seconds
                nextTime = System.DateTime.Now.Ticks/10000;
                if (nextTime-lastTime > 10000)
                {
                    //generate
                    neuralProgram.context.Reset(true);
                    int col = Utils.NextInt(0,COLOR_AREA);

                    bdat = exampleImage.LockBits(new Rectangle(0,0,exampleImage.Width,exampleImage.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
                    unsafe {
                        byte* px = (byte*)bdat.Scan0.ToPointer();
                        int stride = bdat.Stride;

                        float[] ni = neuralProgram.context.inputData,
                                no = neuralProgram.context.outputData;
                        Utils.Fill(ni, 0.0f);
                        float r,g,b;

                        for (int y = 0; y < exampleImage.Height; y++) {
                            for (int x = 0; x < exampleImage.Width; x++) {

                                //put color input
                                ni[col] = 1.0f;

                                //execute neural net
                                neuralProgram.Execute();

                                //remove last color input
                                ni[col] = 0.0f;

                                //sample color from output
                                Utils.Normalize(no);
                                col = Utils.RandomChoice(no);
                                NeuronIDToColor(col, out r, out g, out b);

                                //put pixel in image
                                int pxInd = x*3+y*stride;
                                px[pxInd] = (byte)(r*255);
                                px[pxInd+1] = (byte)(g*255);
                                px[pxInd+2] = (byte)(b*255);
                            }
                        }
                    }
                    exampleImage.UnlockBits(bdat);

                    string sname = "generated" + trainer.GetIterations() + ".png";
                    exampleImage.Save(sname);
                    System.Console.WriteLine("Iterations: " + trainer.GetIterations() + ", Loss: " + trainer.GetLoss() + ". Generated image saved to '"+sname+"'.");
                    
                    lastTime = nextTime;
                }
            }
        }


        //color conversion
        public static int ColorToNeuronID(float r, float g, float b)
        {
            int ir = (int)(r * COLOR_PRECISION),
                ig = (int)(g * COLOR_PRECISION),
                ib = (int)(b * COLOR_PRECISION);
            if (ir == COLOR_PRECISION) ir--;
            if (ig == COLOR_PRECISION) ig--;
            if (ib == COLOR_PRECISION) ib--;

            return ir+ig*COLOR_PRECISION+ib*COLOR_PRECISION*COLOR_PRECISION;
        }
        public static void NeuronIDToColor(int id, out float r, out float g, out float b)
        {
            r = (id % COLOR_PRECISION)/(float)(COLOR_PRECISION-1);
            g = ((id / COLOR_PRECISION) % COLOR_PRECISION)/(float)(COLOR_PRECISION-1);
            b = (id / (COLOR_PRECISION*COLOR_PRECISION))/(float)(COLOR_PRECISION-1);
        }

        
    }
}
